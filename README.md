MarkSYS Test Project 002
========================

### Installation Steps:
1. fork the repository
2. checkout forked repository to your local machine
3. run `composer install` (Composer must be installed. More details [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx))
4. set database parameters in `app/config/parameters.yml`
5. run application. Details [here](http://symfony.com/doc/current/setup.html#running-your-symfony-application)
6. more info on installing and running Symfony application [here](http://symfony.com/doc/current/setup.html)

Once done, send us a link to your repository containing the completed assignment. Specify
branch, if different from master. Describe what you have done with the assignment.

### Setup
First, I checked the version requirements in composer.json. I didnt see what
version of mysql was being used, but after some trial and error I set up a mysql5.6.
I then built the table using:
 php bin/console doctrine:schema:update --force

### Coding
I am not familiar with php and mysql is rusty. I spent a majority of the time
figuring out what classes did and what types were returned from the symfony framework. I built a getMax() function in csvFileRepository, which got the max
from number of rows in a table. I then created findRow() which retrieves one
row of data from the database. I looped over findRow() based on the max number
of rows and then store the data in a 2d array.
I then pass the array into setHeaderRow() which sets the first row as the header.
Next I pass the array to setCellRows() and loop through the data.
These functions now set the rows with data provided, instead of static data.

### Bonus
I did not attempt the bonus requirements.
