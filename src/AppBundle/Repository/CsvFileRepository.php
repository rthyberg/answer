<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CsvCell;
use AppBundle\Entity\CsvFile;
use Doctrine\ORM\EntityRepository;

class CsvFileRepository extends EntityRepository
{
    public function getFile($uuid)
    {
        /** @var CsvFile $csvFile */
        $csvRow = [];
        $csvFile = $this->findFile($uuid);

        // Set max for current csv file
        $max = $this->getMax($csvFile->getId());
        // retrive row data for each row
        for($i = 1; $i <= $max[1]; $i++)
        {
          $csvRow[$i] = $this->findRow($csvFile->getId(), $i);
        }
        $len = count($csvRow);
        $csvFile->setHeaderRow($csvRow);
        $csvFile->setCellRows($csvRow, $len);

        return $csvFile;
    }

    public function findFile($uuid)
    {
        $dql = "SELECT csv ";
        $dql .= "FROM " . CsvFile::class . " csv ";
        $dql .= "WHERE csv.uuid = :uuid";

        $em = $this->getEntityManager();
        $query = $em->createQuery($dql);
        $query->setParameter('uuid', $uuid);

        $result = $query->getResult();

        if ($result) {
            return $result[0];
        }

        return null;
    }

    private function getMax($csvId)
    {
      /* Pulls max row of a table */
      $dql = "SELECT max(cells.row) ";
      $dql .= "FROM " . CsvCell::class . " cells ";
      $dql .= "WHERE cells.csvFile = :id";

      $em = $this->getEntityManager();
      $query = $em->createQuery($dql);
      $query->setParameter('id', $csvId);
      return $query->getResult()[0];
    }

    private function findRow($csvId, $row)
    {
        /* Pulls one row of cell data */
        $dql = "SELECT cells ";
        $dql .= "FROM " . CsvCell::class . " cells ";
        $dql .= "WHERE cells.csvFile = :id AND cells.row = :row ";
        $em = $this->getEntityManager();
        $query = $em->createQuery($dql);
        $query->setParameter('id', $csvId);
        $query->setParameter('row', $row);
        $result = $query->getResult();

        if ($result) {
            return $result;
          }
          return null;
    }
}
